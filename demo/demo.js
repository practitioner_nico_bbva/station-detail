import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons/coronita-icons.js';
import './css/demo-styles.js';
import '../station-detail.js';

// Include below here your components only for demo
// import 'other-component.js'

const response = {
    "currentStatus": {
        "station_id": "2",
        "num_bikes_available": 0,
        "num_bikes_available_types": {
            "mechanical": 0,
            "ebike": 0
        },
        "num_bikes_disabled": 2,
        "num_docks_available": 18,
        "num_docks_disabled": 0,
        "is_installed": 1,
        "is_renting": 1,
        "is_returning": 1,
        "last_reported": 1566828937,
        "is_charging_station": false,
        "status": "IN_SERVICE",
        "lastUpdate": {
            "date": "26/08/2019",
            "hour": "11:15:37"
        }
    },
    "stationInfo": {
        "geolocation": {
            "latitude": -34.5924233,
            "longitude": -58.3747151
        },
        "rental_methods": [
            "KEY",
            "TRANSITCARD",
            "PHONE"
        ],
        "groups": [
            "RETIRO"
        ],
        "_id": "5d5dd12ee711396810d22dd4",
        "station_id": "2",
        "__v": 0,
        "address": "Ramos Mejia, Jose Maria, Dr. Av. & Del Libertador Av.",
        "altitude": 0,
        "capacity": 20,
        "description": "002 - Retiro I",
        "icon": "./resources/images/atm.svg",
        "post_code": "11111"
    }
}

let currentStationDetail = document.getElementById('stationDetail');
currentStationDetail.currentStatus = response.currentStatus;
currentStationDetail.stationInfo = response.stationInfo;