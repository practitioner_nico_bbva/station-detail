import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './station-detail-styles.js';
import '@cells-components/cells-icon';

/**
This component visual utilizado para mostrar el detalle de la estación seleccionada. 

Example:

```html
<station-detail></station-detail>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class StationDetail extends LitElement {
  static get is() {
    return 'station-detail';
  }

  // Declare properties
  static get properties() {
    return {
      currentStatus: { type: Object },
      stationInfo: { type: Object }
    };
  }

  // Initialize properties
  constructor() {
    super();
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('station-detail-shared-styles').cssText}
    `;
  }

  /**
   * Retorna un string en base a una clave.
   * @param {String} type Clave del objeto para retornar el string correspondiente.
   */
  getIcon(type) {
    return {
      "KEY": 'coronita:uniquekey',
      "TRANSITCARD": 'coronita:transfercard',
      "PHONE": 'coronita:mobile'
    }[type];
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <section>
        <!-- <cells-map id="cellsMap" location-icon="http://www.myiconfinder.com/uploads/iconsets/32-32-89fdce5084dbe77556cf99f7b22ae7e8-pin.png" api-key="AIzaSyAAoZgZSu47skks2tq0U_mQoEgeOCvSLgs"></cells-map> -->

        <div class="section-detail-information">
          <div>
            <h1>${ this.stationInfo.description}</h1>
            <h3> ${ this.stationInfo.address} </h3>
          </div>
          <div class="section-detail-information-status">
            <span class="status-icon" ></span>
            <span class="date"> ${ this.currentStatus.lastUpdate.date } - ${  this.currentStatus.lastUpdate.hour} </span>
          </div>
        </div>
        <ul class="slot-bicycle">
          <li>
            <h3> Bicicletas</h3>
            <ul>
              <li>
                <span>Disponible:</span> <span>${ this.currentStatus.num_bikes_available}</span>
              </li>
              <li>
                <span>Deshabilitadas:</span> <span>${ this.currentStatus.num_bikes_disabled}</span>
              </li>
            </ul>
          </li> 
          <li>
            <h3> Slots</h3>
            <ul>
              <li>
                <span>Disponible:</span> <span>${ this.currentStatus.num_docks_available}</span>
              </li>
              <li>
                <span>Deshabilitadas:</span> <span>${ this.currentStatus.num_docks_disabled}</span>
              </li>
            </ul>
          </li>
        </ul>
        <h2 class="capacity"> Esta estación cuenta con ${ this.stationInfo.capacity} bicicletas en total </h2>
        <div class="rent-method">
          <h2>Podés retirar la bici con</h2>
          <ul class="rent-method-list">
            ${ this.stationInfo.rental_methods.map((method) => {
              return html`
                <li class="method-element">
                <cells-icon icon="${ this.getIcon(method)}"> </cells-icon> 
                </li>
            `})}
          </ul>
        </div>
      </section>
    `;
  }
}

// Register the element with the browser
customElements.define(StationDetail.is, StationDetail);
