import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --station-detail; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit;
  color: #484848; }

.section-detail-information {
  padding: 0 15px; }
  .section-detail-information h3 {
    font-size: 12px; }

ul {
  padding: 0;
  margin: 0; }

li {
  list-style: none; }

span {
  display: inline-block; }

.section-detail-information-status {
  display: flex;
  flex-flow: row nowrap;
  align-items: center; }
  .section-detail-information-status .status-icon {
    width: 15px;
    height: 15px;
    background: green;
    display: inline-block;
    border-radius: 50%;
    margin-right: 10px; }
  .section-detail-information-status .date {
    font-size: 11px; }

.slot-bicycle {
  display: flex;
  flex-flow: row nowrap;
  margin-top: 30px; }
  .slot-bicycle > li {
    flex: 1 1 auto;
    padding: 0 10px; }
    .slot-bicycle > li > h3 {
      text-align: center;
      margin-top: 0;
      margin-bottom: 10px; }
    .slot-bicycle > li > ul {
      font-size: 12px;
      text-align: left;
      max-width: 110px;
      margin: 0 auto;
      display: flex;
      flex-flow: column nowrap; }
      .slot-bicycle > li > ul > li {
        display: flex;
        justify-content: space-between;
        text-align: left; }
        .slot-bicycle > li > ul > li:not(:last-child) {
          margin-bottom: 5px; }
        .slot-bicycle > li > ul > li span:first-child {
          width: 90px;
          display: inline-block; }
        .slot-bicycle > li > ul > li span:last-child {
          color: #00adff;
          font-weight: bold; }
    .slot-bicycle > li:first-child {
      border-right: 1px solid black; }

.capacity {
  margin: 30px 0;
  font-size: 12px;
  text-align: center;
  color: #00adff;
  border: 1px solid #00adff;
  padding: 10px;
  border-left: none;
  border-right: none; }

.rent-method h2 {
  font-size: 16px;
  text-align: center;
  margin-bottom: 15px;
  text-transform: uppercase; }

.rent-method .rent-method-list {
  display: flex;
  flex-flow: row nowrap; }
  .rent-method .rent-method-list li {
    flex: 1 1 auto;
    text-align: center;
    padding: 15px 30px; }
    .rent-method .rent-method-list li:not(:last-child) {
      border-right: 1px solid #bdbdbd; }
`;